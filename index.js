// alert("Ca va?")

function displayMsgToSelf(){
	console.log("Don't text him back")
};

// invoke 10 times
// send your cons

displayMsgToSelf();

// While Loop
	/*
		Syntax:
		while(condition){
			statement
		};
	*/

// let count = 20;

// while(count !== 0){
// 	displayMsgToSelf();
// 	count--;
// };

let num = 1

while(num <= 5){
	console.log(num);
	num++
}

// Do-While Looop
/*
	Syntax:
	do {
		statement
	} while(condition)
*/

let doWhilecounter = 1;

do {
	console.log(doWhilecounter)
} while(doWhilecounter === 0)

let number = Number(prompt("Give me a number"))

do {
	console.log("Do While: " + number)
	number += 1
} while(number < 10)

/*
	Mini-Activity
	Create a do-while loop which will be able to show the nubmers in the console from 1-20 in order

	Send your outputs in Hangouts
*/

activityCounter = 1;

do {
	console.log(activityCounter)
	activityCounter ++
} while(activityCounter <= 20)

// For Loop
	/*
		for(initialization; condition;
		finalExpression(increment/decrement);){
	
			statement
		}
	*/

for(let count = 0; count <= 10; count++){

	console.log("For Loop count: " + count);
};

let name = "AlEx";

for(let i = 0; i < name.length; i++){

	if(
		name[i].toLowerCase() == "a" ||
		name[i].toLowerCase() == "e" ||
		name[i].toLowerCase() == "i" ||
		name[i].toLowerCase() == "o" ||
		name[i].toLowerCase() == "u"
	){
		console.log(3);
	} else {

		console.log(name[i])
	}
};

// Continue and Break

for(let count = 0; count <= 20; count++){

	if(count % 2 === 0){

		continue;
	};

	console.log("Continue and Break: " + count);

	if(count > 10){

		break;
	};
};

let myName = "alexandro";

for(let i = 0; i < myName.length; i++){

	console.log(myName[i]);

	if(myName[i].toLowerCase() === "a"){

		console.log("Continue to next iteration")

		continue
	}

	if(myName[i].toLowerCase() === "d"){

		break;
	};
};